<?php
namespace Simpleton;

function bootstrap()
{
	// Execute route
	return route(
		isset($_GET['route']) ?
		$_GET['route'] :
		Config::get('controller_default')
	);
}

function option()
{
	return call_user_func_array( 'Simpleton\Config::option', func_get_args() );
}

function noCache()
{
	header('Expires: -1'); // Proxies.		
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	header('Cache-Control: no-cache, no-store, must-revalidate, max-age=0'); // HTTP 1.1.
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache'); // HTTP 1.0.
	header('Connection: close');
}

/**
 * Get/set if this is an AJAX request
 */
function ajax( $ajax = NULL )
{
	if ( $ajax !== NULL )
	{
		Config::$_ajax = $ajax;
	}
	elseif ( Config::$_ajax === NULL )
	{
		if ( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
		{
			Config::$_ajax = TRUE;
		}
		elseif ( isset($GLOBALS['ajax']) )
		{
			Config::$_ajax = $GLOBALS['ajax'];
		}
		elseif ( isset($_REQUEST['ajax']) )
		{
			Config::$_ajax = $_REQUEST['ajax'];
		}
		else
		{
			Config::$_ajax = FALSE;
		}
	}
	
	return Config::$_ajax;
}

function route( $request = NULL, array $data = array() )
{
	$request    = $request ? $request : Config::get('controller_default');
	$parts      = explode( '/', $request );
	$controller = $parts[0];
	$method     = '';
	
	if ( isset($parts[1]) )
	{
		$method = $parts[1];
		
		if ( isset($parts[2]) )
		{
			$_data = array_slice( $parts, 2 );
			
			for ( $i = 0; $i < count($_data); $i += 2 )
			{
				if ( $_data[$i] )
				{
					$value = NULL;
					if ( isset($_data[$i + 1]) )
					{
						$value = $_data[$i + 1];
					}
					
					$data[$_data[$i]] = $value;
				}
			}
		}
	}
	
	if ( ! $method )
	{
		$method = Config::get('method_default');
	}
	
	if ( Config::get('request_update') )
	{
		$_REQUEST['route']      = $controller . '/' . $method;
		$_REQUEST['controller'] = $controller;
		$_REQUEST['method']     = $method;
	}
	
	return call( $controller, $method, $data );
}

function call( $controller, $method, array $data = array() )
{
	if ( $controller == 'favicon.ico' )
	{
		return FALSE;
	}
	
	try
	{
		$controller = name( $controller );
		$controllerClass = "{$controller}Controller";
		
		if ( ! class_exists($controllerClass) )
		{
			$include_file = controller( $controller );
			if ( file_exists($include_file) )
			{
				include $include_file;
			}
		}
		
		$C = new $controllerClass();
		
		if ( ! method_exists($C, $method) )
		{
			throw new Exception( 'Method does not exist: ' . $method );
		}
		
		if ( Config::get('request_merge') )
		{
			$data = array_merge( $_REQUEST, $data );
		}
		
		return $C->$method( $data );
	}
	catch( Exception $e )
	{
		trigger( 'System.404', array($controller, $method, $data) );
		throw new Exception( 'Page not found: ' . $controller . '/' . $method );
	}	
}

function autoload( $class )
{
	if ( ! Config::get('autoload') )
	{
		return;
	}
		
	$include_file = '';
	
	if ( ! $include_file && Config::get('autoload_controller') && substr($class, -10) == 'Controller' )
	{
		$class = substr( $class, 0, strlen($class) - 10 );
		$include_file = controller( $class );
		$include_file = file_exists($include_file) ? $include_file : '';
	}
	
	if ( ! $include_file && Config::get('autoload_model') )
	{
		$include_file = model( $class );
		$include_file = file_exists($include_file) ? $include_file : '';
	}
	
	if ( ! $include_file && Config::get('autoload_library') )
	{
		$include_file = library( $class );
		$include_file = file_exists($include_file) ? $include_file : '';
	}
	
	if ( $include_file && file_exists($include_file) )
	{
		include $include_file;
	}
	else
	{
		throw new Exception( 'Could not autoload ' . $class );	
	}
}


// ! Event system

/**
 * Add an event callback
 */
function on( $event, $callback )
{
	if ( is_callable($callback) )
	{
		if ( ! isset(Config::$_event[$event]) )
		{
			Config::$_event[$event] = array();
		}
		
		Config::$_event[$event][] = $callback;
		
		return TRUE;
	}
	
	throw new Exception( 'Callback is not callable: ' . (string)$callback );	
	
	return FALSE;
}

/**
 * Remove an event callback
 */	
function off( $event, $callback )
{
	if ( $callback === FALSE )
	{
		unset( Config::$_event[$event] );
		return TRUE;
	}
	else
	{
		foreach( Config::$_event[$event] as $i => $_callback )
		{
			if ( $_callback === $callback )
			{
				unset( Config::$_event[$event][$i] );
				return TRUE;
			}
		}
	}
	
	return FALSE;
}

/**
 * Trigger all registered event callbacks
 */
function trigger( $event, array $arguments = array() )
{
	if ( isset(Config::$_event[$event]) )
	{
        foreach( Config::$_event[$event] as $_callback )
        {
			$return = call_user_func_array( $_callback, array(&$arguments) );
			if ( $return === FALSE )
			{
				break;
			}
        }
        
        return $arguments;
	}
	
	return $arguments;
}


// ! Path shorthands

function name( $basename )
{
	$basename = path( $basename );
	$basename = str_replace( DIRECTORY_SEPARATOR, Config::get('separator_name'), $basename );
	return $basename;
}

function path( $basename, $separator = NULL )
{
	if ( $separator !== FALSE && empty($separator) )
	{
		$separator = Config::get( 'separator_default' );
	}
	
	if ( ! is_array($separator) )
	{
		if ( is_string($separator) )
		{
			$separator = array( $separator );
		}
		else
		{
			$separator = array();
		}
	}
	
	$separator = array_merge( $separator, array('\\', '\\\\', '/', '//') );
	$basename = str_replace( $separator, DIRECTORY_SEPARATOR, $basename );
	
	return $basename;
}

function model( $model, $parse = TRUE )
{
	return Config::get('path') . '/' . Config::get('folder_model') . '/' . ($parse?path($model, Config::get('separator_model')):$model) . '.' . Config::get('extension_model');
}

function view( $view, $parse = TRUE )
{
	return Config::get('path') . '/' . Config::get('folder_view') . '/' . ($parse?path($view, Config::get('separator_view')):$view) . '.' . Config::get('extension_view');
}

function controller( $controller, $parse = TRUE )
{
	return Config::get('path') . '/' . Config::get('folder_controller') . '/' . ($parse?path($controller, Config::get('separator_controller')):$controller) . '.' . Config::get('extension_controller');
}

function layout( $layout, $parse = TRUE )
{
	return Config::get('path') . '/' . Config::get('folder_layout') . '/' . ($parse?path($layout, Config::get('separator_layout')):$layout) . '.' . Config::get('extension_layout');
}

function library( $library, $parse = TRUE )
{
	return Config::get('path') . '/' . Config::get('folder_library') . '/' . ($parse?path($library, Config::get('separator_library')):$library) . '.' . Config::get('extension_library');
}


// ! Error handling

function handleError( $errno, $errstr, $errfile, $errline )
{
    if ( Config::get('handle_fatal') || ! Config::get('handle_error') || ! (error_reporting() & $errno) )
    {
        return FALSE;
    }
	
	throw new \Exception( $errstr, $errno );
	
	return TRUE;
}

function handleException( $e, $fatal = FALSE )
{
	trigger( 'System.Exception', array($e, $fatal) );
	
	if ( Config::get('handle_exception') || ($fatal && Config::get('handle_fatal')) )
	{
		call( Config::get('controller_error'), Config::get('method_default'), array('exception' => $e) );
	}
}

function handleShutdown()
{
	if ( ! Config::get('handle_fatal') )
	{
		return;
	}
	
	$error = error_get_last();
	if ( $error )
	{
		$e = new \Exception(
			$error['message'],
			$error['type']
		);

		handleException( $e, TRUE );
	}
}

class Config
{
	public static $_ajax;
	public static $_event;
	private static $_option = array(
		'autoload'              => TRUE,
		'autoload_controller'   => TRUE,
		'autoload_model'        => TRUE,
		'autoload_library'      => TRUE,
		
		'separator_name'        => '_',
		'separator_default'     => '_',
		'separator_model'       => '',
		'separator_view'        => FALSE,
		'separator_controller'  => '-',
		'separator_layout'      => FALSE,
		'separator_library'     => '',

		'request_update'        => TRUE,
		'request_merge'         => TRUE,
		'method_default'        => 'index',
		'controller_default'    => 'index',
		'controller_error'      => 'error',
		
		'handle_fatal'          => FALSE,
		'handle_exception'      => TRUE,
		'handle_error'          => TRUE,
		
		'path'                  => '',
		'folder_model'          => 'models',
		'folder_view'           => 'views',
		'folder_controller'     => 'controllers',
		'folder_layout'         => 'layouts',
		'folder_library'        => 'libraries',
		
		'extension_model'       => 'php',
		'extension_view'        => 'phtml',
		'extension_controller'  => 'php',
		'extension_layout'      => 'phtml',
		'extension_library'     => 'php',
	);
	
	public static function option()
	{
		switch ( func_num_args() )
		{
			case 2:
				self::set( func_get_arg(0), func_get_arg(1) );
				break;
			
			case 1:
				$key = func_get_arg( 0 );
				if ( is_array($key) )
				{
					self::set( $key );
				}
				else
				{
					return isset(self::$_option[$key]) ? self::get($key) : NULL;
				}
				break;
			
			default:
				return self::get();
				break;
		}
	}
	
	public static function get( $key = NULL )
	{
		if ( $key )
		{
			return self::$_option[$key];
		}
		
		return self::$_option;
	}
	
	public static function set( $key, $value = NULL )
	{
		if ( is_string($key) )
		{
			if ( isset(self::$_option[$key]) && is_array(self::$_option[$key]) )
			{
				self::$_option[$key] = array_merge( self::$_option[$key], $value );
			}
			else
			{
				self::$_option[$key] = $value;
			}
		}
		elseif ( is_array($key) )
		{
			self::$_option = array_merge( self::$_option, $key );
		}
		else
		{
			return FALSE;
		}
		
		return TRUE;
	}
}

class Exception extends \Exception {}

Config::set( 'path', realpath(__DIR__ . '/..') );

// Setup error/exception handling
register_shutdown_function( 'Simpleton\handleShutdown' );
set_exception_handler( 'Simpleton\handleException' );
set_error_handler( 'Simpleton\handleError' );
// Auto loader
spl_autoload_register( 'Simpleton\autoload' );
