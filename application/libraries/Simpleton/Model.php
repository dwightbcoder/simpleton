<?php
namespace Simpleton;

/**
 * Base model class with all our magic
 */
class Model
{
	/**
	 * Populate the instance with values if supplied
	 */
	public function __construct(array $options = null)
	{
		if ( is_array($options) )
		{
			$this->setOptions( $options );
		}
	}

	/**
	 * Handle method calls that are not defined explicitly
	 */
	public function &__call( $method, $args )
	{
		$name = '_' . $method;
		
		if ( ! property_exists( $this, $name ) )
		{
			throw new \Exception( 'Invalid property: ' . $method );
		}
		
		if ( count($args) )
		{
			$this->$name = $args[0];
			return $this;	
		}
		
		return $this->$name;
	}
	
	/**
	 * Redirect set calls to the variable method
	 */
	public function __set( $name, $value )
	{
		throw new \Exception( 'Invalid usage, use ' . $name . '( $value ) method instead.' );
	}
	
	/**
	 * Redirect get calls to the variable method
	 */
	public function __get( $name )
	{
		throw new \Exception( 'Invalid usage, use ' . $name . '() method instead.' );
	}
	
	/**
	 * Set multiple variables en masse
	 */
	public function setOptions( array $options )
	{
		$vars = array_keys( get_object_vars($this) );
		
		foreach ( $options as $key => $value )
		{
			if ( in_array( "_$key", $vars) )
			{
				if ( method_exists($this, $key) )
				{
					$this->$key( $value );
				}
				else
				{
					$this->__call( $key, array($value) );
				}
			}
		}
		
		return $this;
	}
	
	public function toArray( $deep = TRUE, $all = FALSE )
	{
		$vars = array_keys( get_object_vars($this) );
		$data = array();

		foreach ( $vars as $var )
		{
			$var = substr( $var, 1 );
			$data[$var] = $this->$var();
			
			if ( $data[$var] === NULL && ! $all )
			{
				unset( $data[$var] );
			}
			elseif ( is_object($data[$var]) && method_exists($data[$var], 'toArray') )
			{
				if ( $deep )
				{
					$data[$var] = $data[$var]->toArray( $deep, $all );
				}
				else
				{
					unset( $data[$var] );
				}
			}
		}
    	
	    return $data;
	}
}
