<?php
// For backwards compatibility with static class versions of Simpleton
namespace Simpleton;

class System
{
	public static function bootstrap       () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function option          () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function noCache         () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function ajax            () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function route           () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function call            () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function autoload        () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function on              () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function off             () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function trigger         () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function name            () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function path            () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function model           () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function view            () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function controller      () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function layout          () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function library         () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function handleError     () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function handleException () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
	public static function handleShutdown  () { return call_user_func_array( 'Simpleton\\'.__FUNCTION__, func_get_args() ); }
}

try
{
	class_alias( 'Simpleton\System', 'Simpleton' );
	class_alias( 'Simpleton\System', 'S' );
}
catch( \Exception $e ){}
