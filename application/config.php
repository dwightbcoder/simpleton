<?php
include __DIR__ . '/libraries/Simpleton.php';

// Custom error handling
ini_set( 'display_errors', TRUE );

Simpleton\on( 'System.Exception', function( $args )
{
	list( $e, $fatal ) = $args;
	error_log( 'Exception = ' . $e->getMessage() );
});

Simpleton\on( 'System.404', function( $args )
{
	list( $controller, $method ) = $args;
	error_log( '404 = ' . $controller . '/' . $method );
});

// Database
//ModelMapper::pdo( array('DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASS') );
