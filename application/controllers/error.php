<?php
class errorController
{
	function index( $options = array() )
	{
		$exception = NULL;
		extract( $options, EXTR_IF_EXISTS );
		if ( ! $exception )
		{
			$exception = new Exception( 'Unknown error' );
		}
		
		if ( Simpleton\ajax() )
		{
			header( 'Content-Type: application/json' );
			echo json_encode(
			array(
				'status' => 'ERROR',
				'error'  => array( 'message' => $exception->getMessage() )
			));
		}
		else
		{
			if ( headers_sent() )
			{
				include Simpleton\view( 'error' );
			}
			else
			{
				$view = 'error';
				include Simpleton\layout( 'default' );
			}
		}
	}
}
